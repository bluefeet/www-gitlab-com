---
layout: markdown_page
title: "Distribution Team Triage"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Common links

* [Engineering Team Triage](/handbook/engineering/issue-triage/)

## Triaging Issues

Triaging issues involves investigating and applying labels and milestones to issues.

[Issues filter](https://gitlab.com/gitlab-org/omnibus-gitlab/issues?assignee_id=0&milestone_title=No+Milestone&scope=all&sort=created_date&state=opened)

Issues that need triaging are considered as follows:
  - They have No Milestone
  - They have No Author assigned
  - They do **not** have the following labels applied:
    * awaiting feedback
    * for scheduling

Issues are considered **partially** triaged if they have been assigned `for scheduling` or `awaiting feedback` labels.

Issues are **fully** triaged when they have been assigned a Milestone, even if it is `Backlog`

An issue that has been assigned to a user, but has no milestone, is not triaged, but is considered the responsibility of that user, and is not part of our triage queue at this time.

## Issue severity

See the [CE documentation](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#severity-labels-s1-s2-etc) for an explanation of the severity labels.

## Label glossary

| Label | What it means | How to handle it |
| - | - | - |
| awaiting feedback | Information has been requested from the user | If no reply has been received in two weeks, the issue can be closed. |

## Response templates

Copy and paste into issues where appropriate

#### For problems not related to package installation and configuration

If someone is asking for support in the omnibus-gitlab project, point them to the correct place to look

```
This doesn't appear to be an issue with omnibus-gitlab itself. This can be the case when installation and `gitlab-ctl reconfigure` run went without issues, but your GitLab instance is still giving 500 error page with an error in the log.

You can check for ways to get help at the [GitLab website](/getting-help/).

/close
```

#### For issues that lack enough information

If someone opened a ticket without enough information, make sure they use the `Bug` template, and fill it in

```
We can't reproduce the issue with the information you provided here.

Can you please use our `Bug` template to help gather more details?

1. Click on the pencil icon near the issue title to edit the description
1. From the **Choose a template** drop down, select the **Bug** template
1. Read the template, and provide as much information as you can that we ask for
1. Click on the **Save changes** button to apply your changes.
1. Add a comment `/unlabel ~"awaiting feedback"` to let us know that the issue is ready for another look

/label ~"awaiting feedback"
```

#### For issues with no reply

If an issue has been labeled `awaiting feedback` for two weeks, and we haven't received a response, it can be closed

```
We haven't heard back from you, so we're going to go ahead and close the issue.

If you're still experiencing the problem, please re-open the issue and provide the requested information.

/close
```
