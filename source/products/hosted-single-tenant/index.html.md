---
layout: markdown_page
title: "Hosted Single-Tenant"
---

## Hosted Single-Tenant GitLab Instances

GitLab now runs
[GitLab Hosted (a.k.a. GitHost)](/gitlab-hosted) product for existing customers. We are not accepting 
_new_ customers since we recognize that GitHost 
[does not support all of GitLab's features](/gitlab-hosted/#githost-unsupported-features), 
leading to a sub-optimal user experience.

GitLab Inc will not continue to build out a hosted product for single-tenant 
GitLab instances since it is not in line with our stated [strategy and sequence](/strategy/#sequence) 
and because such a product needs a dedicated and prolonged engineering effort.
Instead, we recognize that our strength lies in providing the best possible 
experience for on-premise (i.e. self-hosted) GitLab instances as well as making 
[GitLab.com](https://gitlab.com) (the world's largest multi-tenant instance) highly 
performant with all the features that GitLab Enterprise Edition offers.

Therefore, if you are interested in a hosted [single-tenant](/handbook/glossary/#single-tenant)
GitLab instance - instead of either relying on GitLab.com or hosting your own
GitLab instance - we advise you to do so through third party providers such as the
ones [listed below](#hosted-single-tenant-providers).

## Providers of Hosted (Single-Tenant) GitLab Instances
{: #hosted-single-tenant-providers}

_If you are a provider and wish to have your name and website added to this list,
please submit a merge request to [this page](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/products/hosted-single-tenant/index.html.md)_

- Add list of partner companies here.
