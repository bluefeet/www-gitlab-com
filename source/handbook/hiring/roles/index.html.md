---
layout: markdown_page
title: "Roles"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Role

For each role at GitLab, there should be one position description as it is the single source of truth. A role has the url /roles/department/title and contains the following paragraphs:

- Overview
- Levels (junior, intermediate, senior, staff, etc.) which describe the requirements for each level
- [Specializations](/roles/specialist) (CI/CS, distributed systems, Gitaly) relevant to that role
- Hiring process
- Apply
- About GitLab
- Compensation

The role does not contain:

- Locations (EMEA, Americas, APEC)
- [Expertises](/team/structure/#expert) since these are free form.

The position description will be used _both_ for the [Vacancy Creation Process](/handbook/hiring/vacancies/#vacancy-creation-process), as well as serving as the requirements that team members and managers alike use in conversations around career development and performance management.

## New Role Creation

If a hiring manager is creating a new role within the organization, the hiring manager will need to create the role. If this is a role that already exists (for example, Gitaly Developer would use the Developer position description), update the current position description to stay DRY. If the compensation for the role is the same as one already in `roles.yml`, you should just update the specialty, do not create a new role.

1. Create the relevant page in `/roles/[department]/[name-of-role]`, being sure to use only lower case in naming your directory if it doesn't already exist, and add it to the correct department subdirectory.
1. The file type should be `index.html.md`.
1. Add each paragraph to the position description, for an example see the [developer role](/roles/engineering/developer)
1. Assign the Merge Request to your manager, executive leadership, and finally the CEO to merge. Also, cc `@gl-peopleops` for a compensation review.
1. Once the merge request has been merged, the People Ops Analyst will propose a compensation benchmark for the role. This is a Google Doc that lists the compensation for this role from as many relevant sources as we can find (LinkedIn, Glassdoor, and HR data providers), we'll select the median compensation. In case there is an even number of sources the People Ops Analyst will make a selection.
1. The People Ops Analyst will send an a merge request to the CEO to add the benchmark to the `roles.yml` file including the name or organization codename for source we selected. The description of the the merge request has a link to the role and lists the title of the Google Doc with the analysis.
1. After the merge by CEO the Compensation Calculator](/handbook/people-operations/global-compensation) shonw at the bottom of the role will automatically update.
