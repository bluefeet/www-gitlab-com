---
layout: markdown_page
title: Support Engineering Handbook
---

## Welcome to the GitLab Support Handbook
{: .no_toc}


----

### On this page
{:.no_toc}

- TOC
{:toc}

## Support Engineering At GitLab

We are on the vanguard of helping our customers, from single-instance Omnibus deployments to large 30 Node High Availability set ups. This variety means you will always be on your toes working with technologies ranging from AJAX request parsing, Docker, Linux file permissions, Rails, and many more. Due to this extreme variablity, it's core that we try and keep our processes as lean as possible. 

## Our Processes

There are three core tennants that we should keep in mind that articulate our responsiblities:

- [How to Prioritize Tickets](/handbook/support/support-engineering/prioritizing-tickets.md)
- How to Submit issues to Product/Development
- How to Submit Code to the GitLab Application

The above are links to the appropriate sections of our handbook which further outline how each work.

## How We're Doing

The [Zendesk Insights dashboard](https://gitlab.Zendesk.com/agent/reporting/analytics/period:0/dashboard:buLJ3T7IiFnr) lists the activity for all of our current channels and summarizes the last 30 days (Zendesk login required).

## Our Meetings

Support has 3 meetings a week. Tuesday APAC Support call which will cover Metrics/Demos/Open format questions. Tuesday AMER call, which is focused on Demos and solving challenging tickets as a group. Lastly, the Friday AMER call which is focused on metrics/open format questions. This is how we coordinate and help us all grow together.

## Additional Resources for the Support Team

### Breach Hawks

Breach hawks are members of the support team who help the rest of the team keep an eye out for nearly-breaching tickets, so that they can be responded to in a timely manner. Of course, any and all members of the Support Team will have a sense of how many tickets are close to breaching and how high the load is on any given day. But it can happen that you're deep into a ticket, a customer call, etc., unaware of the impending doom. That's where breach hawks come in: they tackle their tickets, but they also keep an eye out for the team at large and can call in the [Support Turbos](#support-turbo) when needed.

### Support Turbo

Every now and then, it may occur that we come close to breaching our SLAs. To prevent an actual breach from occurring, the Support team can call on the help of several "Support Turbo" developers who are denoted on the [Team Page](/team/).

Support Turbos are separate from the ["fix4all" rotation](/handbook/engineering/fix4all), in that Turbos are on an as-needed basis while the fix4all rotation is a week-long commitment per person. Anyone within the support team can call Turbos when there is a need for it. Turbos should be called by pinging specific people via Slack. If you are in doubt who is available, you can use `@turbos`, but this may lead to a bystander effect.

The support team calls for help when necessary via Slack. Support Turbos should treat requests to solve breaching tickets with high priority. If a Turbo is working on something that cannot wait (e.g. security release, critical regressions, direction issues with near deadlines), the Turbo should ensure that someone else fields the request for Turbos by checking with the development lead or VP of Engineering.

Further guidelines when rolling up your sleeves to do Turbo work:

- Turbos should attempt to pick up "single touch" tickets (though this is not always easy to recognize) whenever possible
- Since Turbos log in using a generic agent, there are some special considerations:
   - Pro-actively sign the tickets with your name, or leave an internal note so that the rest of the team knows which individual provided a response. This is helpful to be able to reach out and gain context, etc.
   - Do not [assign tickets](https://support.zendesk.com/hc/en-us/articles/203690956) to the generic agent account. Leave the assignee as the "group", otherwise these tickets will get lost.
   - Please remember that people may pick up any ticket after you've replied. Adding any issues, docs, or follow-up expectations for a reply as an internal comment would be really beneficial for the person picking it up after you.
