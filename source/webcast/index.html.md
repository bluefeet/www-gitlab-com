---
layout: markdown_page
title: "Webcasts"
---

## On this page
{:.no_toc}

- TOC
{:toc}  


## Upcoming Webcasts  

### GitLab for Enterprises
This is a reoccuring demo webcast that is held every two weeks on Wednesday morning. You'll get a first-hand look at how GitLab integrates version control, project management, code review, testing, and deployment to help enterprise teams move faster from planning to monitoring.   

The webcast starts at 8am PDT / 3pm UTC, you can [register](/webcast/gitlab-enterprise-demo/) for any of the upcoming dates: 
- April 25
- May 9
   
   

### Overcoming Barriers to DevOps Automation - a 4-part series
**April 25th**: Let's Talk DevOps and Drawbacks   
**May 9th**: Solving the Collaboration Conundrum: How to Make the DevOps Dream a Reality    
**May 23rd**: Removing Barriers Between Dev and Ops: Setting Up a CI/CD Pipeline   
**June 6th**: Getting Started with CI/CD on GitLab   

Each webcast starts at 9am PDT / 4pm UTC, you can [register](/webcast/devops-automation/) for one or the entire series.

   
      
      
### Scalable app deployment with GitLab and Google Cloud Platform   
**April 26th** - 2 sessions: 7am PDT / 2pm UTC or 11am PDT / 6pm UTC   
[Register](/webcast/scalable-app-deploy) for either session to learn how to unlock scalable app deployment in a few clicks with the GitLab GKE integration.



### Top 5 Takeaways from the 2018 Global Developer Survey   
**April 27th** - 11am ET / 8a PT - presented by DevOps.com   
[Register today](https://goo.gl/35EzDQ) and join us for a discussion with Ashish Kuthiala, Director of Product Marketing at GitLab and DevOps.com founder Alan Shimel to hear their top 5 takeaways from this year's report and what these trends mean for software professionals in 2018. 
